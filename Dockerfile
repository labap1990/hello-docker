FROM openjdk:8
ADD target/hello-docker.jar hello-docker.jar
EXPOSE 8080
ENTRYPOINT ["java",  "-jar", "hello-docker.jar" ]